const fs = require('fs-extra')

const _knex = require('knex')({
	client: 'sqlite3',
	connection: {
		filename: './gradeio.db',
	},
	useNullAsDefault: true
})

class DB {

	removeDBFile() {
		_knex.destroy(function(){
			fs.removeSync('./gradeio.db')
		})
	}

	getKnex() {
		return _knex
	}

}//()

module.exports = new DB()