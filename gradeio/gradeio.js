#!/usr/bin/env node

/* License - Attribution Assurance: You can use as is for free without modification! 
  Or if you want if you iterate on the ideas, you must clearly and publicly attribute that the inspiration came from: http://ANISscale.com/free.
  One way to do that is to list software used in your about section.
  By using this you agree to pay $10,000 per violation of above.
### Sponsored by:
ANISscale ( http://ANISscale.com/free )
Visit us for more free and paid stuff.
Copyright and TM: ANISscale and/or Cekvenich2, all rights reserved.
*/

const VERSION = 'gradeio version 0.6.5'

const commandLineArgs = require('command-line-args')

const fs = require('fs-extra')

const DB = require('./lib/db')
const { v4: uuidv4 } = require('uuid')
const execa = require('execa')

// ////////////////////////////////////

class Grade {
    static _knex

    async insOne(t) {
        await Grade._knex('gradeio').transacting(t).
            insert({pk: uuidv4(), name: "Cekvenich", dat: new Date() })
    }

    constructor() {
        Grade._knex = DB.getKnex()
    }

    async startTest(quiet) {

        await Grade._knex.schema.createTable('gradeio', (table) => {
            table.string('pk')
            table.string('name')
            table.date('dat')
        })

        if(!quiet) {
            console.clear()
            console.log(VERSION)
            console.log('DO NOT INTERUPT THIS TEST UNTILL IT IS DONE')
            console.log('Running!')
        }

        const start = new Date()

        for (let j = 0; j < 1000; j++) {
            const t = await Grade._knex.transaction()
            for (let i = 0; i < 100; i++) {
                await this.insOne(t)
            }//inner
            await t.commit()
            if(!quiet) process.stdout.write('.')
        }//outer

        const end = new Date() - start
        //report:
        if(!quiet) console.log(''+end/1000)
        // times N is 1 million divided by milliseconds
        const res = (end * 10) / 1000
        console.info(res )
        if(!quiet) console.info('Or',res/60, 'minutes.' )

        // count *
        if(!quiet) console.warn(await Grade._knex('gradeio').count())

        DB.removeDBFile()
    }//()

}//class

async function disk(arg) {
    try {
    {const {stdout} = await execa.commandSync('fio --randrepeat=1 --ioengine=libaio --direct=1 --gtod_reduce=1 --name=test.del --filename=test.del --bs=4k --iodepth=64 --size=4G --readwrite=randrw --rwmixread=75')
    console.log(stdout)}
	
	fs.removeSync('test.del')

    } catch (err) {
        console.error("ERROR")
        console.error(err)
        process.exit(-1)
    }
}//()

// ///////////////////////////////////////////////////
async function grade(quiet, arg) {
	const g = new Grade(quiet)
    await g.startTest(quiet)
}

const optionDefinitions = [
    { name: 'gradeio', defaultOption: true },
    { name: 'quiet', alias: 'q', type: Boolean },
	{ name: 'disk', alias: 'd', type: Boolean },
]

const argsParsed = commandLineArgs(optionDefinitions)
let arg = argsParsed.gradeio

if (argsParsed.quiet)
    grade(true, arg)
else if (argsParsed.disk)
    disk(arg)
else 
    grade(false, arg)
