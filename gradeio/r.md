# gradeio

```npx gradeio``` - measures IO, as number of seconds it takes to insert 1 Million records into a DB file. It leverages SQLite.

```npx gradeio -q``` - run in a quiet mode, will less output.
or sudo npm i -g gradeio, depending on your node setup.


```npx gradeio -d``` - runs regular fio.


It should be used to compare IO of one host to another fost, similar to fio.

### Sponsored by:
ANISscale ( http://ANISscale.com/free )
Visit us for more free and paid stuff.  