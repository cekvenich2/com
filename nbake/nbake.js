#!/usr/bin/env node

/* License - Attribution Assurance: You can use as is for free without modification! 
  Or if you want if you iterate on the ideas, you must clearly and publicly attribute that the inspiration came from: http://ANISscale.com/free.
  One way to do that is to list software used in your about section.
  By using this you agree to pay $10,000 per violation of above.
### Sponsored by:
ANISscale ( http://ANISscale.com/free )
Visit us for more free and paid stuff.
Copyright and TM: ANISscale and/or Cekvenich2, all rights reserved.
*/

const os = require('os')
console.clear()
console.log('nbake version 0.6.24', os.homedir())

const commandLineArgs = require('command-line-args')
const fs = require('fs-extra')
const execa = require('execa')
const fetch = require('make-fetch-happen').defaults({
    cachePath: '/tmp' // path where cache will be written (and read)
  })
const si = require('systeminformation')
const publicIp = require('public-ip')

const optionDefinitions = [
    { name: 'nbake', defaultOption: true },

    { name: 'python', alias: 'y', type: Boolean },
    { name: 'apt',    alias: 'a', type: Boolean },
    { name: 'npm',    alias: 'n', type: Boolean },
    { name: 'editor', alias: 'e', type: Boolean },
    { name: 'info',   alias: 'i', type: Boolean },
    { name: 'caddy',  alias: 'c', type: Boolean },
    { name: 'sysd',   alias: 's', type: String, multiple: true  }, // to restart

    { name: 'php',   alias: 'h', type: Boolean },
    { name: 'back',  alias: 'b', type: Boolean },
    { name: 'front', alias: 'f', type: Boolean },

	{ name: 'git', alias: 't', type: Boolean },

]
const argsParsed = commandLineArgs(optionDefinitions)
let arg = argsParsed.nbake

if (argsParsed.apt)
    apt(arg)
else if (argsParsed.python) // needed for node-gyp
    python(arg)
else if (argsParsed.caddy)
    caddy(arg)
else if (argsParsed.npm)
    npm(arg)
else if (argsParsed.editor)
    editor(arg)
else if (argsParsed.info)
    info(arg)
else if (argsParsed.php)
    php(arg)
else if (argsParsed.back)
    back(arg)
else if (argsParsed.sysd)
    sysd(arg)
else if (argsParsed.git) // undocumented
    git(arg)

else help(arg) 

function help() {
    console.log('Instuctions:', 'http://npmjs.com/package/nbake')
}

// ///////////////////////////////////////////////////
async function git(arg) {
    console.log('nbake -t ')
	await _foo('git status')

	try{
    	await _foo('git add *')
	} catch(e) {} // OK to fail
	try{
    	await _foo('git commit -am "ok"')
	} catch(e) {} // OK to fail
    _foo('git push')
}


async function sysd() {
    const arg = argsParsed.sysd.join(' ')
    console.log('nbake -s '+ arg)
    await _upn()

    await _foo('sudo systemctl '+arg)
}

async function back(arg) {
    console.log('nbake --back')
    await _upn()

    // https://www.npmjs.com/package/tiged
    await _foo('degit --force gitlab:cekvenich2/ex /root/webSrv')

    // the sample app needs npm install for it to run
    {const {stdout} = await execa.commandSync('npm i', {cwd:'/root/webSrv/logSrv'})
    console.log(stdout)}

}//()

async function php(arg) {
    console.log('nbake --php')
    await _upn()

    await _foo('sudo add-apt-repository ppa:ondrej/php')
    await _foo('sudo apt install -y php7.4-fpm php7.4-cli -yq')

    await _foo('sudo systemctl start php7.4-fpm')
    await _foo('sudo systemctl enable php7.4-fpm')

    await _foo('systemctl status php7.4-fpm.service')
    await _foo('netstat -pl | grep php')
}

async function maria(arg) {
    console.log('nbake --maria')
    await _upn()

    try {

    {const {stdout} = execa.commandSync('sudo apt update -yq')
    console.log(stdout)}
    {const {stdout} = execa.commandSync('sudo DEBIAN_FRONTEND=noninteractive apt upgrade -yq')
    console.log(stdout)}
    {const {stdout} = execa.commandSync('sudo apt install mariadb-server mariadb-backup -yq')
    console.log(stdout)}

    try {
        await _mfoo('CREATE USER \'root\'@\'%\' IDENTIFIED BY \'change\'')
    } catch(e) {
        // ok to fail above
    }
    await _mfoo('GRANT ALL PRIVILEGES ON *.* TO \'root\'@\'%\' WITH GRANT OPTION')
    await _mfoo('FLUSH PRIVILEGES;')

    // mysql_secure_installation:
    {const {stdout} = execa.commandSync('echo -e "y\ny\nchange\nchnage\ny\nn\nn\ny" | sh /usr/bin/mysql_secure_installation')
    console.log(stdout)}    // {const {stdout} = await execa.commandSync('npm i -g esdoc')
    // console.log(stdout)}
    await _mfoo('FLUSH PRIVILEGES;')

    // /etc/mysql/mariadb.conf.d/50-server.cnf
    {const {stdout} = execa.commandSync("sudo sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mysql/mariadb.conf.d/50-server.cnf", {shell:true})
    console.log(stdout)}
    {const {stdout} = execa.commandSync('sudo systemctl enable mariadb.service', {shell:true})
    console.log(stdout)}
    {const {stdout} = execa.commandSync('sudo systemctl restart mariadb.service', {shell:true})
    console.log(stdout)}
    await _foo('systemctl status mariadb.service')

    {const {stdout} = execa.commandSync('sudo cat /etc/mysql/mariadb.conf.d/50-server.cnf', {shell:true})
    console.log(stdout)}
    await _mfoo('SELECT User, Host FROM mysql.user')
    console.log()
    {const {stdout} = execa.commandSync('sudo netstat -ntlup | grep maria', {shell:true})
    console.log(stdout)}

    console.log(await publicIp.v4())
    console.log("OK", "The root user should work remote with password 'change', once you login remotely via dBeaver or such you should change it")
    } catch (err) {
        console.error("ERROR", 'You may want to start with a brand new Ubuntu and try again')
        console.error(err)
        process.exit(-1)
    }
}

async function info(arg) {
    console.log('nbake --info')

    await _upn()
	await _foo('npm config list')

    try {
    si.diskLayout().then(data => console.log('disks', data)) 
    si.fsSize().then(data => console.log('disk', data))
    si.mem().then(data => console.log('Free mem in G', data.free/1000000000))
    si.osInfo().then(data => console.log('linux', data.kernel))
    console.log(await publicIp.v4())
    {const {stdout} = execa.commandSync('sudo ufw status verbose')
    console.log(stdout)}

    {const {stdout} = await execa.commandSync('npm -v')
    console.log(stdout)}
    {const {stdout} = execa.commandSync('python --version')
    console.log(stdout)}
    {const {stdout} = execa.commandSync('node -v')
    console.log(stdout)}

    // list services:
    //{const {stdout} = execa.commandSync('systemctl list-units --type=service')
    //console.log(stdout)}
   
    } catch (err) {
        console.error("ERROR")
        console.error(err)
        process.exit(-1)
    }
}

// install VS server. Don't run VS code on your PC/laptop, only in the cloud:
async function editor(arg) {
    console.log('nbake --editor')
    await _upn()

    try {
    await _curl('https://code-server.dev/install.sh','/tmp/vscode.sh') 
    {const {stdout} = execa.commandSync('sh /tmp/vscode.sh', {shell:true})
    console.log(stdout)}
    {const {stdout} = execa.commandSync('sudo systemctl enable --now code-server@$USER', {shell:true})
    console.log(stdout)}

    setTimeout(()=>{
        _editorReConfig()
    },3000)

    } catch (err) {
        console.error("ERROR")
        console.error(err)
        process.exit(-1)
    }
}
async function _editorReConfig() {
    await _foo("sudo sed -i 's/127.0.0.1/0.0.0.0/g' "+os.homedir()+'/.config/code-server/config.yaml')
    await _foo("sudo sed -i 's/8080/9091/g' "+os.homedir()+'/.config/code-server/config.yaml')
    
    {const {stdout} = execa.commandSync('sudo systemctl restart code-server@$USER.service', {shell:true})
    console.log(stdout)}
    {const {stdout} = execa.commandSync('systemctl status code-server@$USER.service', {shell:true})
    console.log(stdout)}
    {const {stdout} = execa.commandSync('cat ~/.config/code-server/config.yaml', {shell:true})
    console.log(stdout)}

    console.log()
    console.log(await publicIp.v4())
    console.log("Open browser on port 9091, password in ~/.config/code-server/config.yaml )")
    console.log("(Or better: as configured by Caddyfile, ideally with https)")
    console.log('editor OK')
}//()

async function npm(arg) {
    console.log('nbake --npm')
    try {
    const cur = process.cwd()
    process.chdir('/tmp')// else n may not

    await _foo('npm set progress=false')

    {const {stdout} = await execa.commandSync('npm i -g npm')
    console.log(stdout)}
    // install latest version of n"
    {const {stdout} = await execa.commandSync('npm i -g --force n')
    console.log(stdout)}
    {const {stdout} = await execa.commandSync('n -p i lts')
    console.log(stdout)}

    await _foo('sudo rm -rf /usr/bin/node')
    await _foo('sudo ln -s /root/n/bin/node /usr/bin/node')
    
	// re-do npm after node lts
    {const {stdout} = await execa.commandSync('npm i -g npm')
    console.log(stdout)}
    // https://github.com/nodejs/node-gyp 
    {const {stdout} = await execa.commandSync('npm i -g node-gyp')
    console.log(stdout)}

	await _foo('npm i -g nexe')
	await _foo('npm i -g zx')
	await _foo('npm install -g prebuild')

    await _foo('npm uninstall -g node-pre-gyp')
    await _foo('npm i -g @mapbox/node-pre-gyp')
	
	try {
    	await _foo('sudo DEBIAN_FRONTEND=noninteractive apt install gyp -yq')
	} catch(e) {} // ok to fail
	try {
    	await _foo('sudo DEBIAN_FRONTEND=noninteractive apt install node-gyp-build -yq')
	} catch(e) {} // ok to fail
    
    {const {stdout} = await execa.commandSync('npm config set python python3')
    console.log(stdout)}
    // testing fmw
    {const {stdout} = await execa.commandSync('npm i -g nbake')
    console.log(stdout)}
    {const {stdout} = await execa.commandSync('npm i -g npm-check-updates')
    console.log(stdout)}

    await _foo('npm i -g tap')
    //await _foo('npm i -g pug')

    // https://www.tecmint.com/enable-pm2-to-auto-start-node-js-app/
    {const {stdout} = await execa.commandSync('npm i -g pm2')
    console.log(stdout)}
    await _foo('pm2 startup systemd')
	await _foo('ln -s /root/.pm2/logs')


    {const {stdout} = await execa.commandSync('npm i -g @11ty/eleventy@beta')
    console.log(stdout)}
    {const {stdout} = await execa.commandSync('npm i -g jsdoc-to-markdown')
    console.log(stdout)}
    {const {stdout} = await execa.commandSync('npm i -g javascript-obfuscator')
    console.log(stdout)}
    // mobile: 
    {const {stdout} = await execa.commandSync('npm i -g cordova')
    console.log(stdout)}

	{const {stdout} = await execa.commandSync('npm i -g tiged')
    console.log(stdout)}
    //{const {stdout} = await execa.commandSync('npm i -g snowpack')
    //console.log(stdout)}

	await _foo('npm i -g typescript')

	//await _foo('npm i -g pino-pretty')

	//await _foo('npm i -g npm-run-all')
    //await _foo('npm i -g create-ficus-app')

    {const {stdout} = await execa.commandSync('npm -v')
    console.log(stdout)}
    console.log()
	_foo('sync') // disk sync

    {const {stdout} = execa.commandSync('node -v')
    process.chdir(cur) // go back 

    console.log(stdout)}
    console.log("OK")
    } catch (err) {
        console.error("ERROR")
        console.error(err)
        process.exit(-1)
    }
}

//must be flush left
const _cad= `{
	auto_https disable_redirects
	servers {
			protocol {
					experimental_http3
			}
	}
}
# caddy stop, start, validate, reload
# http://caddyserver.com/docs/caddyfile
# vs code (change SUB.cekvenich.net to your DNS):
e.cekvenich.net {
	reverse_proxy localhost:9091
}
log.cekvenich.net {
	reverse_proxy localhost:2004
}

:8088 {
	# Set this path to your site's directory.
	root * /root/site/public

	# Enable the static file server.
	file_server

	# Or serve a PHP SSR through php-fpm:
	# php_fastcgi localhost:9000
}
`
async function caddy(arg) {
    console.log('nbake --caddy')
    await _upn()

    try {
    {const {stdout} = execa.commandSync('sudo apt install -y debian-keyring debian-archive-keyring apt-transport-https')
    console.log(stdout)}

    // curl start 
    await _curl('https://dl.cloudsmith.io/public/caddy/stable/gpg.key','/tmp/caddy-stable.asc') 
    {const {stdout} = execa.commandSync('sudo mv /tmp/caddy-stable.asc /etc/apt/trusted.gpg.d/caddy-stable.asc')
    console.log(stdout)}

    await _curl('https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt','/tmp/caddy-stable.list') 
    {const {stdout} = execa.commandSync('sudo mv /tmp/caddy-stable.list /etc/apt/sources.list.d/caddy-stable.list')
    console.log(stdout)}
    // curl end

    {const {stdout} = execa.commandSync('sudo apt update -y')
    console.log(stdout)}
    {const {stdout} = execa.commandSync('sudo apt install caddy -y')
    console.log(stdout)}
    {const {stdout} = execa.commandSync('sudo caddy upgrade')
    console.log(stdout)}
    await _foo('sudo ln -s /etc/caddy/Caddyfile ~/Caddyfile')
    await fs.writeFileSync('/etc/caddy/Caddyfile', _cad)
    await _foo('cat ~/Caddyfile')
    await _foo('sudo systemctl start caddy.service')
    
    try { // OK to fail
        await _foo('sudo systemctl restart code-server@$USER.service')
    } catch(e) {}

    console.log()
    console.log("The user home folder will contain the linked Caddyfile that you should edit")
    } catch (err) {
        console.error("ERROR")
        console.error(err)
        process.exit(-1)
    }
}

async function apt(arg) {

    console.log('nbake --apt')
    await _upn()

    // sudo dpkg --configure -a
    try {
    console.log('This does an apt upgrade so the first time it may run for 10 minutes or more, this is normal.')
    {const {stdout} = execa.commandSync('sudo apt update -yq')
    console.log(stdout)}
    {const {stdout} = execa.commandSync('sudo DEBIAN_FRONTEND=noninteractive apt upgrade -yq')
    console.log(stdout)}
    {const {stdout} = execa.commandSync('sudo DEBIAN_FRONTEND=noninteractive apt dist-upgrade -yq')
    console.log(stdout)}
    {const {stdout} = execa.commandSync('sudo apt install build-essential -yq')
    console.log(stdout)}
    {const {stdout} = execa.commandSync('sudo DEBIAN_FRONTEND=noninteractive apt install openssh-client -yq')
    console.log(stdout)}
    {const {stdout} = execa.commandSync('sudo apt install unzip git zsh htop expect wget curl pkg-config net-tools software-properties-common -yq')
    console.log(stdout)}
	await _foo('sudo DEBIAN_FRONTEND=noninteractive apt --reinstall install libc6 libc6-dev -yq')  // needed for nanomsg and lz4
	await _foo('apt install cmake -yq')
	await _foo('apt install nanomsg-utils -yq')
	await _foo('apt install ninja-build')
    {const {stdout} = execa.commandSync('sudo apt install fio -yq')
    console.log(stdout)}

	//lazygit
	_foo(`MY_FLAVOR=Linux_x86_64; curl -s -L $(curl -s https://api.github.com/repos/jesseduffield/lazygit/releases/latest | grep browser_download_url | cut -d '"' -f 4 | grep -i "$MY_FLAVOR") | sudo tar xzf - -C /usr/local/bin lazygit`)

	try {
    await _foo('sudo ln -s /etc/netplan') // /etc/netplan http://github.com/cockpit-project/cockpit/issues/8477#issuecomment-708037727
	} catch (err) {// ok to err
	}

	try{
	await _foo('sudo apt install aptitude') 
	} catch (err) {// ok to err
	}

	await _foo('sudo echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p') 

    await _foo('sudo systemctl disable console-setup') // fixes service

    await _foo('sudo DEBIAN_FRONTEND=noninteractive apt install tuned tuned-utils tuned-utils-systemtap -yq')
    
    await _foo('sudo DEBIAN_FRONTEND=noninteractive apt install chrony -yq')

    setTimeout(async function(){
        {const {stdout} = execa.commandSync('sudo apt autoremove -yq')
        console.log(stdout)}
        try { // ok to fail:
            await _foo('sudo chronyc -a makestep')
        } catch(err) {}
        
        await _foo('sudo chronyc tracking')

    
        await _foo('sudo DEBIAN_FRONTEND=noninteractive apt install cockpit -yq')
      
        console.log()
        console.log("OK", 'You may want to reboot for new Linux to kick in. Also you may to edit the file in /etc/netplan for cockpit based updates (renderer: NetworkManager).')
    
    },1000)
    
    } catch (err) {
        console.error("ERROR")
        console.error(err)
        process.exit(-1)
    }
}

// needed for gyp, that is needed by node:
async function python(arg) {
    console.log('nbake --python')
    await _upn()

    try {
    {const {stdout} = execa.commandSync('sudo apt install python3.9 -yq')
    console.log(stdout)}
    {const {stdout} = execa.commandSync('sudo apt install python3-pip -yq')
    console.log(stdout)}
    {const {stdout} = execa.commandSync('sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 1')
    console.log(stdout)}
    {const {stdout} = execa.commandSync('python --version')
    console.log(stdout)}

	await _foo('sudo apt install python-is-python3')

    console.log()
    console.log("OK")
    } catch (err) {
        console.error("ERROR")
        console.error(err)
        process.exit(-1)
    }
}

// //////
async function _curl(url, file) {
    return new Promise((resolve, reject) => {
    fetch(url)
        .then(res => res.text())
        .then(async (text) => {
            await fs.writeFileSync(file, text)
            console.log(file)
            resolve()
        } )
    })//pro
}
async function _foo(cmd) {
    console.log(cmd)
    const {stdout} = await execa.commandSync(cmd, {shell:true})
    console.log(stdout)
}
async function _mfoo(cmd) {
    let mcmd = 'sudo mysql -e \"' + cmd + ';\"'
    console.log(mcmd)
    const {stdout} = await execa.commandSync(mcmd, {shell:true})
    console.log(stdout)
}

async function _upn() { // update nbake
    try{
        await _foo('npm i -g nbake')
    } catch(err){} // silient!
}//()
