 
# nbake
nbake is a gentle and opinionated devops configuration tool for ubuntu+nodejs environments, that 'bakes' an environment - helping make configuration robust, reproducible and ephemeral. It prevents decay of an environment.
 
#### One magic thing it can do is install VS Code so it run in a browser so you can develop remote: from a single command: npx nbake -e.
 
If your environment or configuration 'drifts' or gets broken, you should create a new Ubuntu server instance, install everything by running the commands above, and do a git clone. You should now be in a known state. Your environment can be reproducible and ephemeral, with code in git and database backed up/restored. It is a bit like Java's SDKMan or pyinvoke.org, except for nodejs/ubuntu.

## How to install
 You need the latest available version of Ubuntu installed, typically running in the cloud. You can pick anything similar to kamatera.com ( https://www.kamatera.com/express/compute/apps.php?tcampaign=35166_374675&bta=35166&nci=5443#app=mySQL), http://vultr.com, http://primcast.com, http://hetzner.com, http://soyoustart.com, Digital Ocean, Astuteinternet.com, Linode, etc. Recommended minimum is 4 gigs of RAM (due to VS Code server).
 
Here is how to setup up n (nodejs version manager), nodejs and nbake on ubuntu:
```
  curl -L https://raw.githubusercontent.com/tj/n/master/bin/n -o n
 
  sudo bash n lts
  sudo npm i -g nbake
```  
or sudo npm i -g nbake, depending on your node setup.
nbake may work with other ways of installing nodejs, but other ways are not supported or tested.
You may need to remove other ways of installing nodejs via ```rm -rf ~/.nvm```. Only 'n', above is supported.

## Basics configs
Here are is how to run nbake (npx prefix is recommended to get the latest version each time), these nbake commands are require to configure node.js properly(for example some node packages need to be able to compile during npm install):

- ```npx nbake --apt```       installs apt packages needed ( such as build-essential). This does an apt upgrade so the first time it may run for 10 minutes or more, this is normal (you could run apt update and apt upgrade ahead if you wish). It also installs cockpit on port:9090 - so you can use the console from a browser. (http://ip:9090 and then your root and password). You may want to reboot for new Linux to kick in. Also you may to edit the file in /etc/netplan for cockpit based updates (renderer: NetworkManager after 'version 2').

- ```npx nbake -y```         installs and configures python v3 ( needed for node-gyp that is used by some npm packages).

- ```npx nbake --npm```       installs global recommended npm packages ( as listed http://cdn.jsdelivr.net/npm/nbake@latest/nbake.js at function npm()). Needs python and apt installed ahead.
Or just use one dash with first char of the command, eg ```npx nbake -n``` for --npm. You should run these commands from time to time to update to latest versions to reduce 'version drift' between system. Also adds pm2 process manager.

- ```npx nbake --editor```    installs VS Code IDE as a server, that you should use insted of a local IDE via a browser. Open browser on port 9091, password in ~/.config/code-server/config.yaml, or better configure Caddyfile for https. (aside I find it easier to use gitlabs in linux, so I migrated away from github). If you use the editor, you need at least 4 gigs of RAM, or you can disable the service.
You may want to add spell check, editorconfig plugins and set white space to visible all in VS Code.

- ```npx nbake --caddy```     installs caddy http/https server (needed for https and more). The user home folder will contain the linked Caddyfile that you should edit. Likely you need a DNS, I recommend Porkbun.com or http://easydns.com (and I recommend against GoDaddy).
 
### How does it work?
- http://cdn.jsdelivr.net/npm/nbake@latest/nbake.js
 
## More configs
- ```npx nbake --info```  gives some system info. (You can |less or |more).
- ```npx nbake --back```  installs back end nodejs example webApp.
- ```npx nbake -s arg```  runs systemd service command: systemctl + arg

## Secure
You should secure your box via ufw or such - you could only allow post 80 and 443. Also change the DB password, linux password and VS Code Serv password. And consider using CDN (like Bunny.net ) as it hides the IP from the public. CDN that also improves performance.

## Advanced
 
- ```npx nbake -h```     installs PHP
- Example Front End Web App: https://gitlab.com/cekvenich2/fe
 
## More 
- https://gitlab.com/cekvenich2/com/-/blob/main/nbake/more.md


